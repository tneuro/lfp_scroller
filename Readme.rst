LFP Scroller
============

A fast & light scroller based on `pyqtgraph <http://www.pyqtgraph.org/>`_, Enthought `traitsui <http://docs.enthought.com/traitsui/>`_, and HDF5.

Entry point:

.. code-block:: bash

    $ launch_scroller.py

![LFP Scroller screenshot](docs/images/demo.png)